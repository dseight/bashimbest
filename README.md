# README #

### What's this? ###

This app allows you to get best quotes from [bash.im](http://bash.im) for all month starting from 08.2004
and ending at <current_month - 1>.

Quotes are separated by "%" lines. 
This format is used to create a random access file for storing strings using [strfile](http://gsp.com/cgi-bin/man.cgi?section=8&topic=strfile)
which is used in [fortune](http://software.clapper.org/fortune/) program.

### How to use with fortune? ###

1. Create file containing best quotes:

    ```
    $ java -jar BashImBest.jar > ~/bash.im.best
    ```

1. Create random access file:

    ```
    $ strfile -r ~/bash.im.best ~/bash.im.best.dat
    ```

1. Use it in fortune:

    ```
    $ fortune ~/bash.im.best
    ```