package ru.ds8apps.BashImBest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Calendar;

public class Main {

    public static void main(String[] args) {
        // We'll get best quotes starting from 08.2004
        Calendar currentDate = Calendar.getInstance();
        currentDate.set(2004, Calendar.AUGUST, 1);

        // And till now
        Calendar endDate = Calendar.getInstance();
        // We'll move calendar one month back because we will not include current month's best (it's not filled yet)
        endDate.add(Calendar.MONTH, -1);

        while (currentDate.before(endDate)) {
            int year = currentDate.get(Calendar.YEAR);
            int month = currentDate.get(Calendar.MONTH);
            try {
                printBestFor(year, month);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Oops! Seems that there is something with your internet connection");
                break;
            }
            currentDate.add(Calendar.MONTH, 1);
        }
    }

    /**
     * Prints best quotes for specified year and month
     */
    private static void printBestFor(int year, int month) throws IOException {
        final String baseURL = "http://bash.im/bestmonth/";
        String URL = baseURL + year + "/" + month;

        Document doc = Jsoup.connect(URL).get();
        // Get quotes from page
        Elements quotes = doc.getElementsByClass("quote");
        // And print them all
        for (Element quote : quotes) {
            String quoteText = getQuoteText(quote);
            if (quoteText != null) {
                System.out.println(quoteText);
                System.out.println("%");
            }
        }
    }

    /**
     * Receives quote element and returns it's text
     */
    private static String getQuoteText(Element quote) {
        Element quoteTextDiv = quote.getElementsByClass("text").first();
        if (quoteTextDiv != null) {
            quoteTextDiv.select("br").append("\\n");
            return quoteTextDiv.text().replace("\\n", "\n").trim();
        }
        return null;
    }
}
